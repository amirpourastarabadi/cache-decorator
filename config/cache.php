<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Cache Case Separator
    |--------------------------------------------------------------------------
    |
    | Character separating class from method on cache key.
    |
    | Cache key example: "App\Repositories\UserRepo@get:09121111111_2181111111"
    |
    */

    'cache_case_separator' => '@',

    /*
    |--------------------------------------------------------------------------
    | Cache Cases
    |--------------------------------------------------------------------------
    |
    | Specify the items you want to cache

    | Example:
    |
    | 'cache_cases' => [
    |
    |      \App\Repositories\UserRepo::class => [
    |          'find',
    |          'getAdmin',
    |      ],
    |
    | ],
    |
    */

    'cache_cases' => [

        //

    ],

    /*
    |--------------------------------------------------------------------------
    | Cache Special Requirements Items
    |--------------------------------------------------------------------------
    |
    | Determine cache's special tags, prefix key and time.
    | Items of tags, prefix_key and time are optional. If
    |
    | Example:
    |
    | 'cache_special_requirements_items' => [
    |
    |     \App\Repositories\UserRepo::class . '@find' => [
    |         'tags' => ['user', 'user_find'],
    |         'prefix_key' => 'user',
    |         'time' => 2000
    |     ],
    |
    | ],
    |
    */

    'cache_special_requirements_items' => [

        //

    ],
];
