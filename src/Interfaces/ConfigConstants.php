<?php

namespace AlaaTV\CacheDecorator\Interfaces;

use Illuminate\Support\Str;
use mysql_xdevapi\Exception;

abstract class ConfigConstants
{
    const PACKAGE_CONFIG_NAME = 'alaatv_cache_decorator';
    const TARGETS_CONTAINER = self::PACKAGE_CONFIG_NAME . ".cache_special_requirements_items";
    const SEPRATOR = '.cache_case_separator';
    const STATIC_TAGS = '.static_tags';
    const END_DYNAMIC_TAGS = '.tags_with_end_variable';
    const MIDDLE_DYNAMIC_TAGS = '.tags_with_middle_variable';
    const KEY_PREFIX = ".key_prefix";
    const METHOD_VARIABLES = ".method_variables";
    const THIS_VARIABLES = ".this_variables";


    public static function findCacheTarget($params)
    {
        $targetName = $params['class'] . config(self::PACKAGE_CONFIG_NAME . self::SEPRATOR) . $params['method'];
        $target = config(self::TARGETS_CONTAINER . "." . $targetName);
        if(!$target){
            throw new Exception(Str::title("you should first set confing for {$targetName}"));
        }

        return $targetName;

    }

}
