<?php


namespace AlaaTV\CacheDecorator\Interfaces;


interface CacheRequirementsMaker
{
    static function make($params): array;

    static function checkRules($params): bool;

    static function setPrefixes(): array;

    static function setVariables(): array;

    static function generate(): array;

    static function default(): array;
}
