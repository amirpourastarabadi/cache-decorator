<?php


namespace AlaaTV\CacheDecorator\Classes;


use AlaaTV\CacheDecorator\Interfaces\ConfigConstants;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;
use \AlaaTV\CacheDecorator\Classes\CacheRequirementsMaker as CRM;

class KeyMaker extends ConfigConstants
{

    const OPTION = 'key';
    private static $keys = '';

    /**
     * try to find key prefix and generate dynamic keys
     * else generate default keys
     * @param $params
     * @return array
     */
    public static function makeKey($params): string
    {
        if ($keyPrefixes = CRM::findPrerequisites($params, self::OPTION)) {
            $variables = FindVariables::find($params);
            self::generate($keyPrefixes, $variables);
        }

        return self::$keys;
    }

    /**
     * generate keys by concat each prefix by each variable
     * @param $keyPrefixes
     * @param $variables
     */
    private static function generate($keyPrefixes, $variables)
    {
        foreach ($keyPrefixes as $prefix) {
            foreach ($variables as $variable) {
                self::$keys .= $prefix . '_' . $variable;
            }
        }

        if (!self::$keys) {
            self::makeDefaultKeys($params);
        }
    }

    /**
     * generate key by concat class and method
     * @param $params
     * @throws \Exception
     */
    private static function makeDefaultKeys($params)
    {
        $class = $params['class'];
        $method = $params['method'];
        self::$keys .= $class . "_" . $method;
    }


}
