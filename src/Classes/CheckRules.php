<?php


namespace AlaaTV\CacheDecorator\Classes;


use AlaaTV\CacheDecorator\Interfaces\ConfigConstants;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use mysql_xdevapi\Exception;

class CheckRules extends ConfigConstants
{
    private $params;
    private $target;
    private $configMethodVariables;
    private $configThisVariables;
    private $configEndDynamicTags;
    private $configMiddleDynamicTags;
    private $configKeyPrefix;

    public function __construct($params)
    {
        $this->params = $params;
        $this->target = $this->checkTarget();
        $this->configMethodVariables = config(self::TARGETS_CONTAINER . ".$this->target" . self::METHOD_VARIABLES);
        $this->configThisVariables = config(self::TARGETS_CONTAINER . ".$this->target" . self::THIS_VARIABLES);
        $this->configEndDynamicTags = config(self::TARGETS_CONTAINER . ".$this->target" . self::END_DYNAMIC_TAGS);
        $this->configMiddleDynamicTags = config(self::TARGETS_CONTAINER . ".$this->target" . self::MIDDLE_DYNAMIC_TAGS);
        $this->configKeyPrefix = config(self::TARGETS_CONTAINER . ".$this->target" . self::KEY_PREFIX);
    }

    public function check()
    {
        $this->checkTarget();
        $this->checkMethodVariablesRule();
        $this->checkThisVariablesRule();
        $this->checkTagRule();
        $this->checkKeyRule();
    }

    /**
     * check target is in config file
     * @throws \Exception
     */
    public function checkTarget()
    {
        $targetName = $this->params['class'] . config(self::PACKAGE_CONFIG_NAME . self::SEPRATOR) . $this->params['method'];
        $target = config(self::TARGETS_CONTAINER . "." . $targetName);
        if (!$target) {
            throw new \Exception(Str::title("you should first set confing for {$targetName}"));
        }

        return $targetName;
    }

    /**
     * if method variable set in config then at least one of following must set in config too:
     * 1- end_dynamic_tags
     * 2- middle_dynamic_tags
     * 3- key_prefix
     *
     * check method variables introduced in config be match with variables in method call and vice versa
     *
     * @throws \Exception
     */
    public function checkMethodVariablesRule()
    {
        if ($this->configMethodVariables) {
            if (!($this->configEndDynamicTags || $this->configMiddleDynamicTags || $this->configKeyPrefix)) {
                throw new \Exception(Str::title("if you set method_variables in config you should set at least one of:\n
                1- end_dynamic_tags\n
                2- middle_dynamic_tags\n
                3- key_prefix"));
            }
        }

        $keys = array_keys($this->params['params']);
        if ($this->configMethodVariables) {
            foreach ($this->configMethodVariables as $variable) {
                if (!(in_array($variable, $keys))) {
                    throw new \Exception(Str::title("method variables from config not match with
                arguments in method call"));
                }
            }
        }

        if ($this->configMethodVariables) {
            foreach (array_keys($this->params['params']) as $param) {
                if (!(in_array($param, $this->configMethodVariables))) {
                    throw new \Exception(Str::title("method variables from config not match with
                arguments in method call"));
                }
            }
        }

    }

    /**
     * if this variable set in config then at least one of following must set in config too:
     * 1- end_dynamic_tags
     * 2- middle_dynamic_tags
     * 3- key_prefix
     *
     * check this attributes contains attribute set in config
     *
     * @throws \Exception
     */
    public function checkThisVariablesRule()
    {
        if ($this->configThisVariables) {
            if (!($this->configEndDynamicTags || $this->configMiddleDynamicTags || $this->configKeyPrefix)) {
                throw new \Exception(Str::title("if you set this_variables in config you should set at least one of:\n
                1- end_dynamic_tags\n
                2- middle_dynamic_tags\n
                3- key_prefix"));
            }

            $this->checkThisType();
        }
    }

    /**
     * if one of dynamic tag type set in config, then at least one of the followin must set in config too:
     * 1- method variables
     * 2- this variables
     *
     * @throws \Exception
     */
    public function checkTagRule()
    {
        if ($this->configMiddleDynamicTags || $this->configEndDynamicTags) {
            if (!($this->configThisVariables || $this->configMethodVariables)) {
                throw new \Exception(Str::title("if you set " .
                    self::MIDDLE_DYNAMIC_TAGS .
                    " or " . self::END_DYNAMIC_TAGS .
                    " in config, you should set at least one of: 1- " .
                    self::METHOD_VARIABLES .
                    " 2- " .
                    self::THIS_VARIABLES .
                    "."));
            }
        }

    }

    /**
     * if key prefix set in config, then at least one of the followin must set in config too:
     * 1- method variables
     * 2- this variables
     *
     * @throws \Exception
     */
    public function checkKeyRule()
    {
        if ($this->configKeyPrefix) {
            if (!($this->configThisVariables || $this->configMethodVariables)) {
                throw new \Exception(Str::title("if you set " .
                    self::KEY_PREFIX .
                    " in config, you should set at least one of: 1- " .
                    self::METHOD_VARIABLES .
                    " 2- " .
                    self::END_DYNAMIC_TAGS .
                    "."));
            }
        }
    }

    /**
     * type of caller object and key in this_variables config params must match
     * all attributes of this_variables config must exist in caller model
     * @throws \Exception
     */
    public function checkThisType()
    {
        if (isset($this->params['this'])) {
            $thisParamParts = explode("\\", get_class($this->params['this']));
            $thisParamType = Str::lower($thisParamParts[count($thisParamParts) - 1]);
            foreach ($this->configThisVariables as $object => $attributes) {
                if ($object != $thisParamType) {
                    throw new \Exception(Str::title("Object type not compatible with this_variable field in config"));
                }

                foreach ($attributes as $attribute) {
                    if (!Schema::hasColumn($this->params['this']->getTable(), $attribute)) {
                        throw new \Exception(Str::title("$thisParamType model has not \"$attribute\" attribute"));
                    }
                }
            }
        }

    }

}
