<?php


namespace AlaaTV\CacheDecorator\Classes;


use Illuminate\Database\Eloquent\Collection;

class CacheStatic
{
    const PACKAGE_NAME = 'cache_decorator';
    const ACTION_SEPARATOR = '@';

    public static function cache($class, $method, array $params)
    {
        return app(self::PACKAGE_NAME)->call(BuilderWrapper::class . self::ACTION_SEPARATOR . "cache", [
            'params' => [
                'class' => $class,
                'method' => $method,
                'params' => $params,
            ]
        ]);
    }

}
