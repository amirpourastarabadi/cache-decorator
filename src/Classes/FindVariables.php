<?php


namespace AlaaTV\CacheDecorator\Classes;


use AlaaTV\CacheDecorator\Interfaces\ConfigConstants;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;

abstract class FindVariables extends ConfigConstants
{
    private static $thisVariables = [];
    private static $methodVariables = [];

    /**
     * find variables by map confing variable signiture by parametrs sended from user to method and
     * caller object of method in nonstatic method call
     * @param $params
     * @return array
     * @throws \Exception
     */
    public static function find($params): array
    {
        self::$thisVariables = [];
        self::$methodVariables = [];

        if (isset($params['this'])) self::mapThisParmas($params);
        if (isset($params['params'])) self::mapMethodParams($params);

        return array_merge(self::$thisVariables, self::$methodVariables);
    }

    /**
     * find variables by map confing variable signiture by and caller object of method in nonstatic method call
     * @param $params
     * @throws \Exception
     */
    public static function mapThisParmas($params)
    {
        $target = self::findCacheTarget($params);
        $configThisVariables = config(self::TARGETS_CONTAINER . ".$target" . self::THIS_VARIABLES);
        if ($configThisVariables) {
            foreach ($configThisVariables as $object => $attributes) {
                foreach ($attributes as $attribute) {
                    self::$thisVariables[] = $params['this']->$attribute;
                }
            }
        }
    }

    /**
     * find variables by map confing variable signiture by parametrs sended from user to method
     * @param $params
     * @throws \Exception
     */
    public static function mapMethodParams($params)
    {
        $target = self::findCacheTarget($params);
        $configMethodVariables = config(self::TARGETS_CONTAINER . ".$target" . self::METHOD_VARIABLES);
        if ($configMethodVariables) {
            foreach ($configMethodVariables as $variable) {
                if (is_array($params['params'][$variable])) {
                    self::$methodVariables[] = implode('_', $params['params'][$variable]);
                    continue;
                }
                self::$methodVariables[] = $params['params'][$variable];
            }
        }
    }

}
