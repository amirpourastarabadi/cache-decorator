<?php

namespace AlaaTV\CacheDecorator\Classes;

use AlaaTV\CacheDecorator\Interfaces\ConfigConstants;
use Illuminate\Support\Facades\Log;
use mysql_xdevapi\Exception;

abstract class CacheRequirementsMaker extends ConfigConstants
{

    /**
     * check alaatv_cache_decorator config for class and method that comes in $params
     * if yes then check is key prefix or dynamic tag for $target in config
     *
     * @param array $params
     * @param string $option
     * @return boolean|array
     */
    public static function findPrerequisites($params, $option)
    {
        try {
            $target = self::findCacheTarget($params);
            $prerequisites = self::findPrerequisitesInTarget($target, $option);
            return $prerequisites;
        } catch (\Exception $exception) {
            Log::error($exception->getMessage() . "\n\t" . $exception->getFile() . "\n\t" . $exception->getLine());
            return false;
        }
    }

    /**
     * base $option (key/tag) find related prefix from alaatv_cache_decorator config
     * @param string $target
     * @param string $option
     * @return array
     * @throws \Exception
     */
    public static function findPrerequisitesInTarget($target, $option)
    {
        switch ($option) {
            case 'key':
                return self::findKeyPrefix($target);
                break;
            case 'tag':
                return self::findDynamicTagPrefix($target);
                break;
            default:
                throw new Exception();
        }
    }

    /**
     * @param $target
     * @return array
     */
    public static function findKeyPrefix($target): array
    {
        $keyPrefix = config(self::TARGETS_CONTAINER . ".$target" . self::KEY_PREFIX);
        if (!$keyPrefix) {
            return [];
        }
        return $keyPrefix;
    }

    /**
     * @param $target
     * @return array
     */
    public static function findDynamicTagPrefix($target): array
    {
        $dynamicTags = ['end' => [], 'middle' => []];

        $dynamicTags['end'] = config(self::TARGETS_CONTAINER . ".$target" . self::END_DYNAMIC_TAGS);
        $dynamicTags['middle'] = config(self::TARGETS_CONTAINER . ".$target" . self::MIDDLE_DYNAMIC_TAGS);

        return $dynamicTags;

    }

}
