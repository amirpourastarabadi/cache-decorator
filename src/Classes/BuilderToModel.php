<?php

namespace AlaaTV\CacheDecorator\Classes;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class BuilderToModel
{
    const PAGINATE_COUNT = 3;
    const PACKAGE_NAME = 'cache_decorator';
    const ACTION_SEPARATOR = '@';

    public static function first($class, $method, array $params): ?Model
    {
        return app()->call(BuilderWrapper::class . self::ACTION_SEPARATOR ."first", [
            'class' => $class,
            'method' => $method,
            'params' => $params,
        ]);
//        return self::baseBuilder('first', false, $class, $method, $params);
    }

    public static function firstFromCache($class, $method, array $params): ?Model
    {
        return app(self::PACKAGE_NAME)->call(BuilderWrapper::class . self::ACTION_SEPARATOR ."first", [
            'params' => [
                'class' => $class,
                'method' => $method,
                'params' => $params,
            ]
        ]);
//        return self::baseBuilder('first', true, $class, $method, $params);
    }

    public static function getFromCache($class, $method, array $params): ?Collection
    {
        return app(self::PACKAGE_NAME)->call(BuilderWrapper::class . self::ACTION_SEPARATOR ."get", [
            'params' => [
                'class' => $class,
                'method' => $method,
                'params' => $params,
            ]
        ]);
//        return self::baseBuilder('get', true, $class, $method, $params);
    }

    public static function get($class, $method, array $params): ?Collection
    {
        return app()->call(BuilderWrapper::class . self::ACTION_SEPARATOR ."get", [
            'class' => $class,
            'method' => $method,
            'params' => $params,
        ]);
//        return self::baseBuilder('get', false, $class, $method, $params);
    }

    public static function paginate($class, $method, array $params = [], $count = self::PAGINATE_COUNT): LengthAwarePaginator
    {
        return app()->call(BuilderWrapper::class . self::ACTION_SEPARATOR ."paginate", [
            'class' => $class,
            'method' => $method,
            'count' => $count,
            'params' => $params,
        ]);
//        return self::baseBuilder('paginate', false, $class, $method, $params, $count);
    }

    public static function paginateFromCache($class, $method, array $params = [], $count = self::PAGINATE_COUNT): LengthAwarePaginator
    {
        return app(self::PACKAGE_NAME)->call(BuilderWrapper::class . self::ACTION_SEPARATOR ."paginate", [
            'params' => [
                'class' => $class,
                'method' => $method,
                'count' => $count,
                'params' => $params,
            ]
        ]);
//        return self::baseBuilder('paginate', true, $class, $method, $params, $count);
    }

    public static function baseBuilder(string $outputType, bool $beCached, $class, $method, $params, $count = null)
    {
        $params = [
            'class' => $class,
            'method' => $method,
            'params' => $params,
        ];
        if (isset($cout)) {
            $params['count'] = $count;
        }
        if ($beCached) {
            $params = ['params' => $params];
        }
        return app(self::PACKAGE_NAME)->call(BuilderWrapper::class . self::ACTION_SEPARATOR . $outputType, $params);
    }
}
