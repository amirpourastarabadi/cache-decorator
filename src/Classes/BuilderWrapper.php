<?php

namespace AlaaTV\CacheDecorator\Classes;

class BuilderWrapper
{
    /**
     * Important! Please don't change this class methods' parameters title
     */

    const ACTION_SEPARATOR = '@';

    /**
     * @param string $class
     * @param string $method
     * @param array $params
     *
     * @return mixed
     */
    public static function commonActionCall(string $class, string $method, array $params)
    {
        return app()->call($class . self::ACTION_SEPARATOR . $method, $params);
    }

    /**
     * @param string $class
     * @param string $method
     * @param array $params
     *
     * @return mixed
     */
    public static function first(string $class, string $method, array $params)
    {
        return self::commonActionCall($class, $method, $params)->first();
    }

    /**
     * @param string $class
     * @param string $method
     * @param array $params
     *
     * @return mixed
     */
    public static function get(string $class, string $method, array $params)
    {
        return self::commonActionCall($class, $method, $params)->get();
    }

    /**
     * @param string $class
     * @param string $method
     * @param array $params
     * @param int $count
     *
     * @return mixed
     */
    public static function paginate(string $class, string $method, array $params, int $count)
    {
        return self::commonActionCall($class, $method, $params)->paginate($count);
    }

    public static function cache(string $class, string $method, array $params)
    {
        return self::commonActionCall($class, $method, $params);
    }
}
