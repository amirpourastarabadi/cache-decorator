<?php

namespace AlaaTV\CacheDecorator\Classes;

use App\Repositories\UserRepo;
use App\User;
use Illuminate\Support\Str;

class CacheNonStatics
{
    const PACKAGE_NAME = 'cache_decorator';

    public static function cache($object, $method, $params)
    {
        return app(self::PACKAGE_NAME)->callNoneStatic($object, $method, [
            'params' => [
                'class' => get_class($object),
                'method' => $method,
                'params' => $params,
                'this' => $object,
            ]
        ]);
    }
}
