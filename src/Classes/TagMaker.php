<?php


namespace AlaaTV\CacheDecorator\Classes;


use AlaaTV\CacheDecorator\Classes\CacheRequirementsMaker as CRM;
use AlaaTV\CacheDecorator\Interfaces\ConfigConstants;
use App\Notifications\sendLink;

class TagMaker extends ConfigConstants
{
    const OPTION = 'tag';
    private static $tags = [];

    public static function maketag($params)
    {
        if ($prefixes = CRM::findPrerequisites($params, self::OPTION)) {
            $variables = FindVariables::find($params);
            self::generate($prefixes, $variables, $params);
        }
        return self::$tags;
    }

    private static function generate($prefixes, $variables, $params)
    {
        self::makeStaticTags($params);
        if ($prefixes['end']) {
            self::makeEndDynamicTags($prefixes['end'], $variables);
        }
        if ($prefixes['middle']) {
            self::makeMiddleDynamicTags($prefixes['middle'], $variables);
        }

        if (!self::$tags) {
            self::makeDefaultTags($params);
        }

    }

    private static function makeStaticTags($params)
    {
        $target = self::findCacheTarget($params);
        self::$tags = config(self::TARGETS_CONTAINER . ".$target" . self::STATIC_TAGS);
    }

    private static function makeEndDynamicTags($prefixes, $variables)
    {
        foreach ($prefixes as $prefix) {
            foreach ($variables as $variable) {
                if (is_array($variable)) {
                    array_push(self::$tags, $prefix . '_' . implode('_', $variable));
                    continue;
                }
                array_push(self::$tags, $prefix . '_' . $variable);
            }
        }
    }

    private static function makeMiddleDynamicTags($prefixes, $variables)
    {
        foreach ($prefixes as $prefix) {
            foreach ($variables as $variable) {
                $start = explode(' ', $prefix)[0];
                $end = explode(' ', $prefix)[1];
                if (is_array($variable)) {
                    array_push(self::$tags, $start . '_' . implode('_', $variable) . "_" . $end);
                    continue;
                }
                array_push(self::$tags, $start . '_' . $variable . "_" . $end);
            }
        }

    }

    private static function makeDefaultTags($params)
    {
        self::$tags = $params['class'] . "_" . $params['method'];
    }
}
