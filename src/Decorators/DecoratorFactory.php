<?php

namespace AlaaTV\CacheDecorator\Decorators;

use AlaaTV\CacheDecorator\Classes\CacheRequirementsMaker;
use AlaaTV\CacheDecorator\Classes\CheckRules;
use AlaaTV\CacheDecorator\Classes\KeyMaker;
use AlaaTV\CacheDecorator\Classes\TagMaker;
use Closure;
use Composer\Cache;
use Illuminate\Support\Facades\App;

class DecoratorFactory
{
    /**
     * @return Closure
     */
    public static function cache(): Closure
    {
        return self::getDecoratorFactory();
    }

    /**
     * @return Closure
     */
    private static function getDecoratorFactory(): Closure
    {
        return function ($callable) {
            return function ($params) use ($callable) {
//                $dci = CacheRequirementsMaker::keyMaker($params);
                (new CheckRules($params))->check();
                $key = KeyMaker::makeKey($params);
                $tags = TagMaker::maketag($params);

                $cb = function () use ($callable, $params) {
                    return \App::call($callable, $params);
                };
//                $tags = $dci['tags'];
//                $key = $dci['key'];
                $time = 99999;
//                dd($tags, $key, $time);
                $remember = isset($time) && $time > 0 ? 'remember' : 'rememberForever';

                return cache()->tags($tags)
                    ->$remember(...array_filter([$key, $time, $cb], function ($el) {
                        return !is_null($el);
                    }));
            };
        };
    }
}
