<?php

namespace AlaaTV\CacheDecorator\Decorators;

use Closure;
use Illuminate\Support\Str;

class Decorator
{
    /**
     * All of the decorators for method calls.
     *
     * @var array
     */
    protected $globalDecorators = [];

    /**
     * All of the decorator names and definitions.
     *
     * @var array
     */
    protected $decorations = [];

    /**
     * Separate between class and method. e.g. UserRepo@find
     */
    const ACTION_SEPARATOR = '@';

    /**
     * Decorates a callable with a defined decorator name.
     *
     * @param string $callback
     * @param mixed $decorator
     *
     * @return void
     */
    public function decorate($callback, $decorator)
    {
        $this->decorations[$callback][] = $decorator;
    }

    /**
     * Calls a class@method with it's specified decorators.
     *
     * @param string $callback
     * @param array $parameters
     * @param string|null $defaultMethod
     *
     * @return mixed
     */
    public function call($callback, array $parameters = [], $defaultMethod = null)
    {
        if (is_array($callback)) {
            $callback = $this->normalizeMethod($callback);
        }
        $decorators = $this->getDecorationsFor($callback);
        $callback = $this->decorateWith($callback, $decorators);
        return app()->call($callback, $parameters, $defaultMethod);
    }

    /**
     * @param $object
     * @param string $method
     * @param array $parameters
     *
     * @return mixed
     */
    public function callNoneStatic($object, $method, $parameters)
    {
        $callback = get_class($object) . self::ACTION_SEPARATOR . $method;
        $decorators = $this->getDecorationsFor($callback);
        $callback = $this->makeNoneStaticCallback($object, $method, $parameters);


        $callback = $this->decorateWith($callback, $decorators);
        return app()->call($callback, $parameters,);


    }

    /**
     * @param $callback
     *
     * @return string
     */
    private function normalizeMethod($callback)
    {
        $class = is_string($callback[0]) ? $callback[0] : get_class($callback[0]);

        return $class . self::ACTION_SEPARATOR . $callback[1];
    }

    /**
     * @param $callback
     *
     * @return array|mixed
     */
    public function getDecorationsFor($callback)
    {
        return $this->decorations[$callback] ?? [];
    }

    /**
     * @param $callable
     * @param array $decorators
     *
     * @return mixed
     */
    public function decorateWith($callable, array $decorators)
    {
        foreach ($decorators as $decorator) {
            if (is_string($decorator) and !Str::contains($decorator, self::ACTION_SEPARATOR)) {
                $decorator = $this->globalDecorators[$decorator];
            }
            $callable = app()->call($decorator, ['callable' => $callable]);
        }
        return $callable;
    }

    /**
     * @param $object
     * @param string $method
     * @return Closure
     */
    public function makeNoneStaticCallback($object, $method, $parameters): Closure
    {
        $finalParams = [];
        $inputs = $parameters['params']['params'];
        $func = function ($param) {
            return $param->name;
        };

        $method = (new \ReflectionObject($object))->getMethod($method);
        $params = $method->getParameters();
        $params = array_map($func, $params);

        foreach ($params as $param) {
            if (isset($inputs[$param])) $finalParams[] = $inputs[$param];
            else throw new \Exception("Mehtod inputs in method call not match with method signiture");
        }
        return function () use($method, $object, $finalParams){
            return $method->invoke($object, ...$finalParams);
        };
//        return function ($params) use ($object, $method) {
//            return $object->{$method}($params);
//        };
    }
}
