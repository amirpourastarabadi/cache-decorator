<?php

namespace AlaaTV\CacheDecorator\Facade;

use Illuminate\Support\Facades\Facade as LaravelFacade;

/**
 * Class Decorator.
 *
 * @method static decorate($callback, $decorator)
 * @method static call($callback, array $parameters = [], $defaultMethod = null)
 */
class Decorator extends LaravelFacade
{
    protected static function getFacadeAccessor()
    {
        return \AlaaTV\CacheDecorator\Decorators\Decorator::class;
    }
}

