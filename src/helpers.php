<?php

use Illuminate\Database\Eloquent\Model;
use \AlaaTV\CacheDecorator\Classes\BuilderToModel;
use \AlaaTV\CacheDecorator\Classes\CacheNonStatics;
use \AlaaTV\CacheDecorator\Classes\CacheStatic;

const __EMAS = 'emads';

if (!function_exists('first')) {
    function first($class, $method, $params = [])
    {
        return BuilderToModel::first($class, $method, $params);
    }
}

if (!function_exists('firstFromCache')) {
    function firstFromCache($class, $method, $params = [])
    {
        return BuilderToModel::firstFromCache($class, $method, $params);
    }
}

if (!function_exists('get')) {
    function get($class, $method, $params = [])
    {
        return BuilderToModel::get($class, $method, $params);
    }
}

if (!function_exists('getFromCache')) {
    function getFromCache($class, $method, $params = [])
    {
        return BuilderToModel::getFromCache($class, $method, $params);
    }
}

if (!function_exists('paginate')) {
    function paginate($class, $method, $params = [], $count = null)
    {
        return BuilderToModel::paginate($class, $method, $params, $count);
    }
}

if (!function_exists('paginateFromCache')) {
    function paginateFromCache($class, $method, $params = [], $count = null)
    {
        return BuilderToModel::paginateFromCache($class, $method, $params, $count);
    }
}

// use for none static methods
if (!function_exists('cacheNoneStatics')) {
    function cacheNoneStatics($object, $method, $params = [])
    {
        return CacheNonStatics::cache($object, $method, $params);
    }
}

// use for static methods that don't return builder
if (!function_exists('cacheStatic')) {
    function cacheStatic($class, $method, $params = [])
    {
        return CacheStatic::cache($class, $method, $params);
    }
}

