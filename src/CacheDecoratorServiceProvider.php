<?php

namespace AlaaTV\CacheDecorator;

use AlaaTV\CacheDecorator\Classes\BuilderWrapper;
use AlaaTV\CacheDecorator\Classes\CacheStatics;
use AlaaTV\CacheDecorator\Decorators\Decorator;
use AlaaTV\CacheDecorator\Facade\Decorator as DecoratorFacade;
use AlaaTV\CacheDecorator\Decorators\DecoratorFactory;
use Illuminate\Support\ServiceProvider;

class CacheDecoratorServiceProvider extends ServiceProvider
{
    protected $defer = true;

    const PACKAGE_CONFIG_NAME = 'alaatv_cache_decorator';
    const PACKAGE_CONFIG_PATH = __DIR__ . '/../config/cache.php';
    const PACKAGE_NAME = 'cache_decorator';
    const ACTION_SEPARATOR = '@';

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Decorator::class);
        $this->app->singleton(DecoratorFactory::class);
        $this->app->singleton(self::PACKAGE_NAME, Decorator::class);

        // Set key for package's config file as default config file.
        $this->mergeConfigFrom(self::PACKAGE_CONFIG_PATH, self::PACKAGE_CONFIG_NAME);
    }

    public function provides()
    {
        return [Decorator::class, self::PACKAGE_NAME, DecoratorFactory::class];
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // Copy package's config file to app config directory by "vendor:publish" command.
        $this->publishes([
            self::PACKAGE_CONFIG_PATH => config_path(self::PACKAGE_CONFIG_NAME . '.php'),
        ]);

        $decorator = DecoratorFactory::cache();

        DecoratorFacade::decorate(BuilderWrapper::class . self::ACTION_SEPARATOR . "first", $decorator);
        DecoratorFacade::decorate(BuilderWrapper::class . self::ACTION_SEPARATOR . "get", $decorator);
        DecoratorFacade::decorate(BuilderWrapper::class . self::ACTION_SEPARATOR . "paginate", $decorator);
        DecoratorFacade::decorate(BuilderWrapper::class . self::ACTION_SEPARATOR . "cache", $decorator);

        $cacheCases = [];
        if (config()->has(self::PACKAGE_CONFIG_NAME . '.cache_cases') && is_array(config(self::PACKAGE_CONFIG_NAME . '.cache_cases')) && count(config(self::PACKAGE_CONFIG_NAME . '.cache_cases'))) {
            $cacheCases = config(self::PACKAGE_CONFIG_NAME . '.cache_cases');
        }
        foreach ($cacheCases as $class => $methods) {
            if (is_array($methods) && count($methods)) {
                foreach ($methods as $method) {
                    DecoratorFacade::decorate($class . self::ACTION_SEPARATOR . $method, $decorator);
                }
            }
        }
    }
}
